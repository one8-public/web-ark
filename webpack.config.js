/* Webpack 設定檔 */

const path = require('path')
var webpack = require('webpack')

module.exports = {
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
  },
  entry: {
    avatar: ['./src/js/cropper/cropper.min.js', './src/js/cropper/avatar-main.js'],
    plugs: [
      './src/js/marquee/jquery.marquee.min.js',
      './src/js/count/jquery.count.min.js',
      './src/js/panelslider/jquery.panelslider.js',
      './src/js/waypoints/jquery.waypoints.min.js',
      './src/js/validationEngine/languages/jquery.validationEngine-zh_CN.js',
      './src/js/validationEngine/jquery.validationEngine.js',
      './src/js/plugs.js',
    ],
    gamePlugs: [
      './src/js/marquee/jquery.marquee.min.js',
      './src/js/validationEngine/languages/jquery.validationEngine-zh_CN.js',
      './src/js/validationEngine/jquery.validationEngine.js',
      './src/js/gamePlugs.js',
    ],
    iframePlugs: ['./src/js/iframePlugs.js'],
  },
  output: {
    path: path.resolve(__dirname, 'dist/js'),
    filename: '[name].min.js',
  },
  resolve: {
    // alias: {
    //   jquery: path.join(__dirname, '/src/js/jquery-3.1.1.min.js'),
    // },
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
    }),
  ],
}
