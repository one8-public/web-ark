const colors = require('tailwindcss/colors')
module.exports = {
  purge: ['./src/css/*.scss', './src/vendor/*.css', './dist/**/*.html', './dist/*.html', './dist/**/*.vue', './dist/*.vue'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      xs: '375px',
      // => @media (min-width: 375px) { ... }

      xm: '414px',
      // => @media (min-width: 414px) { ... }

      sx: '500px',
      // => @media (min-width: 500px) { ... }

      sm: '640px',
      // => @media (min-width: 640px) { ... }

      md: '768px',
      // => @media (min-width: 768px) { ... }

      lg: '1024px',
      // => @media (min-width: 1024px) { ... }

      xl: '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1536px',
      // => @media (min-width: 1536px) { ... }

      ipx: [
        { 'min-width': '812px', 'max-height': '375px', 'max-width': '812px' },
        { 'max-width': '375px', 'min-height': '812px', 'min-width': '375px' },
      ],
      pixl: [
        { 'min-width': '411px', 'max-height': '960px', 'max-width': '411px' },
        { 'max-width': '960px', 'min-height': '411px', 'min-width': '823px' },
      ],
      duo: [{ 'min-width': '500px', 'min-height': '500px' }],
    },
    extend: {
      width: {
        17: '4.25rem',
        50: '12.5rem',
        '4/25': '16%',
        '9/50': '18%',
        '7.6/10': '76%',
        '8.7/10': '87%',
        '9/10': '90%',
        '9.05/10': '90.5%',
        'screen-h': '100vh',
      },
      height: {
        4.5: '1.125rem',
        '0.7/10': '7%',
        '2/25': '8%',
        '1/10': '10%',
        '1/2': '50%',
        '6.1/10': '61%',
        '16/25': '68%',
        '1/7': '14%',
        '7/10': '70%',
        '9/10': '90%',
        'screen-w': '100vw',
      },
      minHeight: {
        16: '4rem',
        20: '5rem',
      },
      inset: {
        '-16/10': '-160%',
        '-7.2': '-1.8rem',
        '-1/5': '-20%',
        '-1.1/10': '-11%',
        '3/5': '60%',
        '2/5': '40%',
        '1.75/5': '35%',
        '3/10': '30%',
        '2.5/10': '25%',
        '1/5': '20%',
        '3/20': '15%',
        '1.4/10': '14%',
        '1.38/10': '13.8%',
        '1.3/10': '13%',
        '3/25': '12%',
        '1/10': '10%',
        '2/25': '8%',
        '13/200': '6.5%',
        '1.5/25': '6%',
        '1/20': '5%',
        '1/25': '4%',
        '1/50': '2%',
        '3/200': '1.5%',
        '1/200': '0.5%',
      },
      margin: {
        '-18': '-4.5rem',
        '0.6/100': '0.6%',
        '0.8/100': '0.8%',
        '1.5/100': '1.5%',
        '2/100': '2%',
        '5/100': '5%',
        '3/25': '12%',
        '1/6': '16.67%',
        '1/5': '20%',
        '2/6': '33.33%',
        '2/5': '40%',
      },
      padding: {
        0.5: '0.125rem',
        4.5: '1.12rem',
        full: '100%',
        '4/5': '80%',
        '13/20': '65%',
        '7/10': '70%',
        '2/3': '60%',
        '1/2': '50%',
        '3/10': '30%',
        '1/4': '25%',
        '9/50': '18%',
        '17/100': '17%',
        '3/20': '15%',
        '5/100': '5%',
        '4.5/100': '4.5%',
        '4/100': '4%',
        '3/100': '3%',
        '2/100': '2%',
        '1/100': '1%',
      },
      spacing: {
        160: '40rem',
      },
      colors: {
        'main-color': '#004e98',
        'formError-color': '#222',
        'formError-border-color': '#000',
        'dark-purple': '#1e073b',
        'light-pink': '#fd08f4',
        'light-pink-100': '#f0abfc',
        'dark-green': '#207e11',
        'dark-gray': '#031a00',

        purple: colors.purple,
        violet: colors.violet,
        fuchsia: colors.fuchsia,
      },
      fontFamily: {
        body: ['Verdana', 'SimHei', '"Microsoft JhengHei"', 'STHeiti', '"PingFang SC"', 'Arial'],
        'bebas-neue': ['"Bebas Neue"'],
      },
      zIndex: {
        '-1': '-1',
        100: '100',
      },
      backgroundImage: {
        main: "url('../images/bg/bg-main.jpg')",
        'main-2': "url('../images/bg/bg-main-shiny.jpg')",
        menu: "url('../images/bg/bg-menu.png')",
        logo: "url('../images/logo-2.png')",
        'item-game': "url('../images/menu/item-game.png')",
        'item-treasure': "url('../images/menu/item-treasure.png')",
        'item-crown': "url('../images/menu/item-crown.png')",
        'item-coin': "url('../images/menu/item-coin.png')",
        'item-waiter': "url('../images/menu/item-waiter.png')",
        'jp-1': "url('../images/game/jackpot/jp1//bg.png')",
        'jp-2': "url('../images/game/jackpot/jp2/bg.png')",
        '7pk-1': "url('../images/game/control/7pk/bg.jpg')",
        '7pk-1-shangfen': "url('../images/game/control/7pk/btn-shangfen.png')",
        '7pk-1-shangfen-hover': "url('../images/game/control/7pk/btn-shangfen-hover.png')",
        '7pk-1-xiafen': "url('../images/game/control/7pk/btn-xiafen.png')",
        '7pk-1-xiafen-hover': "url('../images/game/control/7pk/btn-xiafen-hover.png')",
        '7pk-1-bet': "url('../images/game/control/7pk/btn-bet.png')",
        '7pk-1-bet-hover': "url('../images/game/control/7pk/btn-bet-hover.png')",
        '7pk-1-open': "url('../images/game/control/7pk/btn-open.png')",
        '7pk-1-open-hover': "url('../images/game/control/7pk/btn-open-hover.png')",
        '7pk-1-xiao': "url('../images/game/control/7pk/btn-xiao.png')",
        '7pk-1-xiao-hover': "url('../images/game/control/7pk/btn-xiao-hover.png')",
        '7pk-1-da': "url('../images/game/control/7pk/btn-da.png')",
        '7pk-1-da-hover': "url('../images/game/control/7pk/btn-da-hover.png')",
        '7pk-1-bibei': "url('../images/game/control/7pk/btn-bibei.png')",
        '7pk-1-bibei-hover': "url('../images/game/control/7pk/btn-bibei-hover.png')",
        '7pk-1-score': "url('../images/game/control/7pk/btn-score.png')",
        '7pk-1-score-hover': "url('../images/game/control/7pk/btn-score-hover.png')",
        '15r-1': "url('../images/game/control/15r/bg.jpg')",
        '15r-1-top': "url('../images/game/control/15r/bg-top.png')",
        '15r-1-bet': "url('../images/game/control/15r/btn-bet.png')",
        '15r-1-bet-hover': "url('../images/game/control/15r/btn-bet-hover.png')",
        '15r-1-start': "url('../images/game/control/15r/btn-start.png')",
        '15r-1-start-hover': "url('../images/game/control/15r/btn-start-hover.png')",
        '15r-1-auto': "url('../images/game/control/15r/btn-auto.png')",
        '15r-1-auto-hover': "url('../images/game/control/15r/btn-auto-hover.png')",
        '15r-1-shangfen': "url('../images/game/control/15r/btn-shangfen.png')",
        '15r-1-shangfen-hover': "url('../images/game/control/15r/btn-shangfen-hover.png')",
        '15r-1-xiafen': "url('../images/game/control/15r/btn-xiafen.png')",
        '15r-1-xiafen-hover': "url('../images/game/control/15r/btn-xiafen-hover.png')",
        '15r-1-stop1': "url('../images/game/control/15r/btn-stop1.png')",
        '15r-1-stop1-hover': "url('../images/game/control/15r/btn-stop1-hover.png')",
        '15r-1-stop2': "url('../images/game/control/15r/btn-stop2.png')",
        '15r-1-stop2-hover': "url('../images/game/control/15r/btn-stop2-hover.png')",
        '15r-1-stop3': "url('../images/game/control/15r/btn-stop3.png')",
        '15r-1-stop3-hover': "url('../images/game/control/15r/btn-stop3-hover.png')",
        '15r-1-stop4': "url('../images/game/control/15r/btn-stop4.png')",
        '15r-1-stop4-hover': "url('../images/game/control/15r/btn-stop4-hover.png')",
        '15r-1-stop5': "url('../images/game/control/15r/btn-stop5.png')",
        '15r-1-stop5-hover': "url('../images/game/control/15r/btn-stop5-hover.png')",
        'marie-1-start': "url('../images/game/control/marie/btn-start.png')",
        'marie-1-start-hover': "url('../images/game/control/marie/btn-start-hover.png')",
        'marie-1-right': "url('../images/game/control/marie/btn-right.png')",
        'marie-1-right-hover': "url('../images/game/control/marie/btn-right-hover.png')",
        'marie-1-left': "url('../images/game/control/marie/btn-left.png')",
        'marie-1-left-hover': "url('../images/game/control/marie/btn-left-hover.png')",
        'marie-1-shangfen': "url('../images/game/control/marie/btn-shangfen.png')",
        'marie-1-shangfen-hover': "url('../images/game/control/marie/btn-shangfen-hover.png')",
        'marie-1-xiafen': "url('../images/game/control/marie/btn-xiafen.png')",
        'marie-1-xiafen-hover': "url('../images/game/control/marie/btn-xiafen-hover.png')",
        'marie-1-xiao': "url('../images/game/control/marie/btn-xiao.png')",
        'marie-1-xiao-hover': "url('../images/game/control/marie/btn-xiao-hover.png')",
        'marie-1-da': "url('../images/game/control/marie/btn-da.png')",
        'marie-1-da-hover': "url('../images/game/control/marie/btn-da-hover.png')",
        'marie-1-fruit': "url('../images/game/control/marie/btn-fruit.png')",
        'marie-1-fruit-hover': "url('../images/game/control/marie/btn-fruit-hover.png')",
        'marie-1-apple': "url('../images/game/control/marie/icon-apple.png')",
        'marie-1-77': "url('../images/game/control/marie/icon-77.png')",
        'marie-1-bell': "url('../images/game/control/marie/icon-bell.png')",
        'marie-1-bar': "url('../images/game/control/marie/icon-bar.png')",
        'marie-1-orange': "url('../images/game/control/marie/icon-orange.png')",
        'marie-1-star': "url('../images/game/control/marie/icon-star.png')",
        'marie-1-litchi': "url('../images/game/control/marie/icon-litchi.png')",
        'marie-1-melon': "url('../images/game/control/marie/icon-melon.png')",
        'marie-1-watermelon': "url('../images/game/control/marie/icon-watermelon.png')",
        'fish-1-head-btn': "url('../images/game/control/fish/bg-head-btn.png')",
        'fish-1-head-btn-2': "url('../images/game/control/fish/bg-head-btn-2.png')",
        'fish-1-head': "url('../images/game/control/fish/bg-head.png')",
        'fish-1-fire': "url('../images/game/control/fish/btn-fire.png')",
        'fish-1-bet': "url('../images/game/control/fish/btn-bet.png')",
        'fish-1-bet-hover': "url('../images/game/control/fish/btn-bet-hover.png')",
        'fish-1-fire-hover': "url('../images/game/control/fish/btn-fire-hover.png')",
        'fish-1-auto': "url('../images/game/control/fish/btn-auto.png')",
        'fish-1-auto-hover': "url('../images/game/control/fish/btn-auto-hover.png')",
        'fish-1-right': "url('../images/game/control/fish/btn-right.png')",
        'fish-1-right-hover': "url('../images/game/control/fish/btn-right-hover.png')",
        'fish-1-left': "url('../images/game/control/fish/btn-left.png')",
        'fish-1-left-hover': "url('../images/game/control/fish/btn-left-hover.png')",
        'fish-1-weapon': "url('../images/game/control/fish/btn-weapon.png')",
        'fish-1-weapon-hover': "url('../images/game/control/fish/btn-weapon-hover.png')",
        'fish-1-select': "url('../images/game/control/fish/btn-fish.png')",
        'fish-1-select-hover': "url('../images/game/control/fish/btn-fish-hover.png')",
        'fish-1-shoot': "url('../images/game/control/fish/btn-fire-shoot.png')",
      },
      backgroundPosition: {
        '-top-10': 'center top -40rem',
      },
      boxShadow: {
        '3xl': '0 10px 15px 1px rgba(0, 0, 0, 0.7)',
        '4xl': '3px 20px 20px 8px rgba(0, 0, 0, 0.7)',
        'dark-inner': 'inset 1px 2px 4px 0 rgba(0, 0, 0, 0.6);',
      },
      rotate: {
        360: '360deg',
      },
      scale: {
        135: '1.35',
      },
      translate: {
        'screen-w': '100vw',
      },
      keyframes: {
        light: {
          '10%': { opacity: '0', transform: 'scale(1)' },
          '12%,14%,22%,30%': { opacity: '0.5' },
          '13%,15%,18%,24%': { opacity: '0.2' },
          '25%,55%': { opacity: '1' },
          '100%': { opacity: '0', transform: 'scale(1.5)' },
        },
        bulbLight: {
          '0%, 50%, 100%': { opacity: '0' },
          '25%, 75%': { opacity: '1' },
        },
        'bulbLight-2': {
          '0%, 50%, 100%': { opacity: '1' },
          '25%, 75%': { opacity: '0' },
        },
        fly: {
          '0%, 100%': { transform: 'translate3d(0, 10px, 0) ' },
          '50%': { transform: 'translate3d(0, 0, 0)' },
        },
        'fly-2': {
          '0%, 100%': { transform: 'translate3d(0, -10px, 0) ' },
          '50%': { transform: 'translate3d(0, 0, 0)' },
        },
        scaleRubber: {
          '0%, 70%, 100%': { transform: 'scaleX(1)' },
          '75%, 85%': { transform: 'scale3d(1.2, 0.9, 1) ' },
          '80%': { transform: 'scale3d(0.9, 1.2, 1)' },
          '90%': { transform: 'scale3d(1, 1.05, 1)' },
          '95%': { transform: 'scale3d(1.05, 1, 1)' },
        },
        scaleRotate: {
          '0%, 100%': { left: '0', transform: 'scale(0.8, 1) rotate(0deg)' },
          '25%': { left: '40px', transform: 'scale(1.2, 0.8) rotate(5deg)' },
          '50%': { left: '0', transform: 'scale(0.8, 1) rotate(0deg)' },
          '75%': { left: '-40px', transform: 'scale(1.2, 0.8) rotate(-10deg)' },
        },
        titleLight: {
          '10%': { opacity: '1' },
          '12%,14%,22%,30%': { opacity: '0.5' },
          '13%,15%,18%,24%': { opacity: '0.2' },
          '25%,55%': { opacity: '0' },
          '100%': { opacity: '1' },
        },
        'blink-1': {
          '0%,50%,to': {
            opacity: '1',
          },
          '25%,75%': {
            opacity: '0',
          },
        },
        'blink-2': {
          '0%,to': {
            opacity: '1',
          },
          '50%': {
            opacity: '.2',
          },
        },
        'jello-horizontal': {
          '0%,to': {
            transform: 'scale3d(1, 1, 1)',
          },
          '30%': {
            transform: 'scale3d(1.25, .75, 1)',
          },
          '40%': {
            transform: 'scale3d(.75, 1.25, 1)',
          },
          '50%': {
            transform: 'scale3d(1.15, .85, 1)',
          },
          '65%': {
            transform: 'scale3d(.95, 1.05, 1)',
          },
          '75%': {
            transform: 'scale3d(1.05, .95, 1)',
          },
        },
        'wobble-hor-bottom': {
          '0%,to': {
            transform: 'translateX(0%)',
            'transform-origin': '50% 50%',
          },
          '15%': {
            transform: 'translateX(-30px) rotate(-6deg)',
          },
          '30%': {
            transform: 'translateX(15px) rotate(6deg)',
          },
          '45%': {
            transform: 'translateX(-15px) rotate(-3.6deg)',
          },
          '60%': {
            transform: 'translateX(9px) rotate(2.4deg)',
          },
          '75%': {
            transform: 'translateX(-6px) rotate(-1.2deg)',
          },
        },
        'slide-in-bck-top': {
          '0%': {
            transform: 'translateZ(700px) translateY(-300px)',
            opacity: '0',
          },
          to: {
            transform: 'translateZ(0) translateY(0)',
            opacity: '1',
          },
        },
        'slide-in-bck-bottom': {
          '0%': {
            transform: 'translateZ(700px) translateY(300px)',
            opacity: '0',
          },
          to: {
            transform: 'translateZ(0) translateY(0)',
            opacity: '1',
          },
        },
        'fade-in-top': {
          '0%': {
            transform: 'translateY(-50px)',
            opacity: '0',
          },
          to: {
            transform: 'translateY(0)',
            opacity: '1',
          },
        },
        'scale-in-center': {
          '0%': {
            transform: 'scale(0)',
            opacity: '1',
          },
          to: {
            transform: 'scale(1)',
            opacity: '1',
          },
        },
        'bounce-in-top': {
          '0%': {
            transform: 'translateY(-500px)',
            'animation-timing-function': 'ease-in',
            opacity: '0',
          },
          '38%': {
            transform: 'translateY(0)',
            'animation-timing-function': 'ease-out',
            opacity: '1',
          },
          '55%': {
            transform: 'translateY(-65px)',
            'animation-timing-function': 'ease-in',
          },
          '72%,90%,to': {
            transform: 'translateY(0)',
            'animation-timing-function': 'ease-out',
          },
          '81%': {
            transform: 'translateY(-28px)',
          },
          '95%': {
            transform: 'translateY(-8px)',
            'animation-timing-function': 'ease-in',
          },
        },
        'tracking-in-expand': {
          '0%': {
            'letter-spacing': '-.5em',
            opacity: '0',
          },
          '40%': {
            opacity: '.6',
          },
          to: {
            opacity: '1',
          },
        },
        showPopup: {
          '0%': {
            transform: 'scale(0.7)',
          },
          '45%': {
            transform: 'scale(1.05)',
          },
          '80%': {
            transform: 'scale(0.95)',
          },
          '100%': {
            transform: 'scale(1)',
          },
        },
      },
      animation: {
        light: 'light 4s ease-in-out infinite',
        bulbLight: 'bulbLight 0.6s ease-in-out infinite ',
        'bulbLight-2': 'bulbLight-2 0.6s ease-in-out infinite ',
        fly: 'fly 3s linear infinite',
        'fly-2': 'fly-2 3s linear infinite',
        scaleRubber: 'scaleRubber 4s ease-in-out infinite',
        scaleRotate: 'scaleRotate 2s linear infinite',
        titleLight: 'titleLight 3s ease-in-out infinite',
        'titleLight-2': 'blink-1 1.5s ease-in-out infinite',
        'blink-2': 'blink-2 0.8s ease-in-out infinite ',
        'wobble-hor-bottom': 'wobble-hor-bottom 0.8s ease infinite',
        'jello-horizontal': 'jello-horizontal 0.8s ease-in-out infinite',
        'slide-in-bck-top': 'slide-in-bck-top 0.7s cubic-bezier(0.250, 0.460, 0.450, 0.940)',
        'slide-in-bck-bottom': 'slide-in-bck-bottom 0.7s cubic-bezier(0.250, 0.460, 0.450, 0.940)',
        'fade-in-top': 'fade-in-top 1.2s cubic-bezier(0.390, 0.575, 0.565, 1.000) forwards',
        'scale-in-center': 'scale-in-center 0.5s cubic-bezier(0.250, 0.460, 0.450, 0.940) forwards',
        'bounce-in-top': 'bounce-in-top 1.1s cubic-bezier(0.165, 0.840, 0.440, 1.000)',
        'tracking-in-expand': 'tracking-in-expand 0.8s ease-out',
        showPopup: 'showPopup 0.3s ease-in-out',
      },
    },
  },
  variants: {
    extend: {
      backgroundImage: ['active'],
      borderWidth: ['last'],
      padding: ['hover', 'focus'],
      rotate: ['active'],
      animation: ['hover', 'group-hover'],
      scale: ['active'],
    },
  },
  plugins: [],
}
