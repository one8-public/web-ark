import 'jquery'

// 頁面載入LOADING;
$(window).on('load', async function () {
  await $('.loading').fadeOut(500)

  await checkSize()
})

// 監聽視窗改變尺寸
window.addEventListener(
  'resize',
  debounce(function (e) {
    checkSize()
  })
)

function debounce(func) {
  var timer
  return function (event) {
    if (timer) clearTimeout(timer)
    timer = setTimeout(func, 200, event)
  }
}

function checkSize() {
  var isRotate = $('.game-container').attr('data-rotate')
  if (isRotate) {
    if ($(window).width() < $(window).height()) {
      $('.game-container').addClass('rotate90')
      $('body').addClass('hasRotate90')
      $('.game-container').css({ width: $(window).height(), height: '100vw' })
    } else {
      $('.game-container').removeClass('rotate90')
      $('body').removeClass('hasRotate90')
      $('.game-container').css({ width: '100vw', height: $(window).height() })
    }
  }
}
