$(function () {
  'use strict'

  var URL = window.URL || window.webkitURL
  var $image = $('#avatar')

  var options = {
    aspectRatio: 1 / 1,
    preview: '.avatar-preview',
    crop: function (e) {},
  }
  var originalImageURL = $image.attr('src')
  var uploadedImageURL

  $image.on({}).cropper(options)

  // Methods
  $('.avatar-control').on('click', '[data-method]', function () {
    var $this = $(this)
    var data = $this.data()
    var $target
    var result

    if ($this.prop('disabled') || $this.hasClass('disabled')) {
      return
    }

    if ($image.data('cropper') && data.method) {
      data = $.extend({}, data) // Clone a new one

      if (data.method === 'rotate') {
        $image.cropper('clear')
      }

      if (data.method != 'crop') {
        $image.cropper(data.method, data.option, data.secondOption)
      }

      if (data.method === 'rotate') {
        $image.cropper('crop')
      }

      switch (data.method) {
        case 'scaleX':
        case 'scaleY':
          $(this).data('option', -data.option)
          break

        case 'destroy':
          if (uploadedImageURL) {
            URL.revokeObjectURL(uploadedImageURL)
            uploadedImageURL = ''
            $image.attr('src', originalImageURL)
          }

          break
        case 'crop':
          $('.avatar-upload .avatar-data').val(
            JSON.stringify($image.cropper('getData'))
          )

          if (typeof goUpload == 'function') {
            goUpload()
          }
          break
      }
    }
  })

  // Keyboard
  $(document.body).on('keydown', function (e) {
    if (!$image.data('cropper') || this.scrollTop > 300) {
      return
    }

    switch (e.which) {
      case 37:
        e.preventDefault()
        $image.cropper('move', -1, 0)
        break

      case 38:
        e.preventDefault()
        $image.cropper('move', 0, -1)
        break

      case 39:
        e.preventDefault()
        $image.cropper('move', 1, 0)
        break

      case 40:
        e.preventDefault()
        $image.cropper('move', 0, 1)
        break
    }
  })

  // Import image
  var $inputImage = $('#avatarInput')

  if (URL) {
    $inputImage.change(function () {
      var files = this.files
      var file

      if (!$image.data('cropper')) {
        return
      }

      if (files && files.length) {
        file = files[0]

        if (/^image\/\w+$/.test(file.type)) {
          if (uploadedImageURL) {
            URL.revokeObjectURL(uploadedImageURL)
          }

          uploadedImageURL = URL.createObjectURL(file)
          $image
            .cropper('destroy')
            .attr('src', uploadedImageURL)
            .cropper(options)
        } else {
          window.alert('Please choose an image file.')
        }
      }
    })
  } else {
    $inputImage.prop('disabled', true).parent().addClass('disabled')
  }
})
