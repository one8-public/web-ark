import 'jquery'
import 'slick-carousel'
import 'twbs-pagination'
import Swiper from 'swiper/bundle'
import ClipboardJS from 'clipboard'
import tippy from 'tippy.js'
import * as Ladda from 'ladda'
import Swal from 'sweetalert2'

// 設定給html可使用
window.$ = window.jQuery = $
window.Swiper = Swiper
window.Swal = Swal
window.Ladda = Ladda
window.ClipboardJS = ClipboardJS

// 頁面載入LOADING;
$(window).on('load', function () {
  $('.loading').fadeOut(500)
})

// 阻止手機網頁長按問題
window.ontouchstart = function (e) {
  e.preventDefault()
}
window.addEventListener('contextmenu', function (e) {
  e.preventDefault()
})

$(function () {
  $('.ladda-button').on('click', function () {
    var l = Ladda.create(document.querySelector('.ladda-button'))
    l.start()
  })
  //   Swal.fire('Hello world!')
  if ($('.nav-btn').length > 0) {
    $('.nav-btn').on('click', function (event) {
      let $this = $(this)
      //取消事件冒泡
      event.stopPropagation()
      checkNav($this)
    })
  }

  /* 跑馬燈*/
  if ($('.marquee').length > 0) {
    $('.marquee').marquee()
  }

  if ($('.game-name').length > 0) {
    $('.game-name').marquee({
      showSpeed: 550,
      pauseSpeed: 3000,
    })
  }

  // BANNER輪播
  if ($('#slider-banner .swiper-container').length > 0) {
    const swiper = new Swiper('#slider-banner .swiper-container', {
      slidesPerView: 1,
      spaceBetween: 30,
      loop: true,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    })
  }
  if ($('#slider-gameList .swiper-container').length > 0) {
    const swiper = new Swiper('#slider-gameList .swiper-container', {
      spaceBetween: 10,
      loop: false,
      breakpoints: {
        330: {
          slidesPerView: 2,
        },
        430: {
          slidesPerView: 3,
        },
        640: {
          slidesPerView: 4,
        },
      },
    })
  }

  // 選單滑動
  if ($('.openNav-btn').length > 0) {
    $('.openNav-btn').panelslider()

    $('.closeNav-btn').on('click', function () {
      $.panelslider.close()
    })
  }
  if ($('.openService-btn').length > 0) {
    $('.openService-btn').panelslider()

    $('.closeService-btn').on('click', function () {
      $.panelslider.close()
    })
  }

  // 點擊展開
  if ($('.ac-pane').length > 0) {
    $('.ac-title').on('click', function (event) {
      $(this).next('.ac-content').slideToggle(200)
      $(this).parent().toggleClass('active')
      if ($(this).parent().find('i').hasClass('fas fa-chevron-down')) {
        $(this).parent().find('i').attr('class', 'fas fa-chevron-up')
      } else {
        $(this).parent().find('i').attr('class', 'fas fa-chevron-down')
      }
    })
  }

  // 頁碼
  if ($('.post-page').length > 0) {
    $('.pages').twbsPagination({
      totalPages: 10,
      startPage: 1,
      visiblePages: 5,
      first: '<i class="fas fa-angle-double-left">',
      prev: '<i class="fas fa-angle-left"></i>',
      next: '<i class="fas fa-angle-right">',
      last: '<i class="fas fa-angle-double-right">',
      onPageClick: function (event, page) {
        console.log(page)
      },
    })
  }

  // 欄位驗證
  if ($('.validate').length > 0) {
    $('.validate').validationEngine('attach', {
      promptPosition: 'bottomLeft',
      scroll: true,
      autoPositionUpdate: true,
    })
  }

  // 顯示密碼
  if ($('.showPwd').length > 0) {
    $('.showPwd').on('click', function (event) {
      let $this = $(this)
      //取消事件冒泡
      event.stopPropagation()
      if ($this.hasClass('isVisible')) {
        $this.removeClass('isVisible')
        $this.find('i').removeClass('fa-eye-slash').addClass('fa-eye')
        $this.parent('.form-group').find('input[type="text"]').prop('type', 'password')
      } else {
        $this.addClass('isVisible')
        $this.find('i').removeClass('fa-eye').addClass('fa-eye-slash')
        $this.parent('.form-group').find('input[type="password"]').prop('type', 'text')
      }
    })
  }

  // 遊戲快捷選單
  if ($('.slider-nav').length > 0) {
    $('.slider-nav').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      centerMode: true,
      focusOnSelect: true,
      infinite: true,
      speed: 300,
      arrows: false,
      dots: false,
      centerPadding: '-10%',
    })

    var elementTop = $('.slider-nav').offset().top
    window.addEventListener('scroll', function (e) {
      if ($(this).scrollTop() > elementTop) {
        $('.quickly-nav').addClass('fixed-nav')
        $('.type-list').addClass('has-quickly')
      } else {
        $('.quickly-nav').removeClass('fixed-nav')
        $('.type-list').removeClass('has-quickly')
      }
    })

    // 快捷滑動
    if ($('.scrollTo').length > 0) {
      $('.scrollTo').on('click', function (event) {
        let $this = $(this)
        // 取得識別碼
        var Identifier = $this.attr('data-to')
        if ($('.quickly-nav').hasClass('fixed-nav')) {
          var patch = $('header').height() + $('.fixed-nav').height()
        } else {
          var patch = $('header').height() + $('#slider-banner').height()

          if ($('.type-list').hasClass('has-quickly') || Identifier == 'machine') {
            patch = patch - parseInt(patch * 0.3)
          }
        }
        // 滑到定點
        $('html,body').animate({ scrollTop: $('#' + Identifier).offset().top - patch }, 800)
      })
    }

    // 連動快捷選單滑動
    if ($('#machine').length > 0) {
      $('#machine').waypoint(
        function (direction) {
          $('.slider-nav').slick('slickGoTo', 0)
        },
        {
          offset: '50%',
        }
      )
    }
    if ($('#live').length > 0) {
      $('#live').waypoint(
        function (direction) {
          if (direction == 'down') {
            $('.slider-nav').slick('slickGoTo', 1)
          } else {
            $('.slider-nav').slick('slickGoTo', 0)
          }
        },
        {
          offset: '50%',
        }
      )
    }
    if ($('#sport').length > 0) {
      $('#sport').waypoint(
        function (direction) {
          if (direction == 'down') {
            $('.slider-nav').slick('slickGoTo', 2)
          } else {
            $('.slider-nav').slick('slickGoTo', 1)
          }
        },
        {
          offset: '50%',
        }
      )
    }
    if ($('#lottery').length > 0) {
      $('#lottery').waypoint(
        function (direction) {
          if (direction == 'down') {
            $('.slider-nav').slick('slickGoTo', 3)
          } else {
            $('.slider-nav').slick('slickGoTo', 2)
          }
        },
        {
          offset: '50%',
        }
      )
    }
    if ($('#electronic').length > 0) {
      $('#electronic').waypoint(
        function (direction) {
          if (direction == 'down') {
            $('.slider-nav').slick('slickGoTo', 4)
          } else {
            $('.slider-nav').slick('slickGoTo', 3)
          }
        },
        {
          offset: '50%',
        }
      )
    }
    if ($('#porker').length > 0) {
      $('#porker').waypoint(
        function (direction) {
          if (direction == 'down') {
            $('.slider-nav').slick('slickGoTo', 5)
          } else {
            $('.slider-nav').slick('slickGoTo', 4)
          }
        },
        {
          offset: '50%',
        }
      )
    }
    if ($('#fish').length > 0) {
      $('#fish').waypoint(
        function (direction) {
          if (direction == 'down') {
            $('.slider-nav').slick('slickGoTo', 6)
          } else {
            $('.slider-nav').slick('slickGoTo', 5)
          }
        },
        {
          offset: '50%',
        }
      )
    }
  }

  // 滑入提示工具
  if ($('.tooltip').length > 0) {
    tippy('.tooltip', {
      animation: 'scale-extreme',
      placement: 'bottom',
    })
  }

  // 判斷是否有要修改密碼，有則顯示再次驗證密碼
  if ($('#confirmPassword').length > 0 && $('input[name="password"]').length > 0) {
    $('input[name="password"]').on('blur', function () {
      if ($('input[name="password"]').val() == '') {
        $('#confirmPassword').hide(200)
      } else {
        $('#confirmPassword').show(200)
      }
    })
  }

  // 數值跳動
  if ($('#cr-jp1').length > 0) {
    $('#cr-jp1').numberRockFloat({
      initNumber: 1126599.0,
      lastNumber: 1126650.51,
      easing: 'linear',
      duration: 15000,
      step: 0.01,
      fixedSize: 2,
    })
  }
  if ($('#cr-jp2').length > 0) {
    $('#cr-jp2').numberRockFloat({
      initNumber: 2126599.0,
      lastNumber: 2126650.81,
      easing: 'linear',
      duration: 18000,
      step: 0.01,
      fixedSize: 2,
    })
  }
  if ($('#cr-jp3').length > 0) {
    $('#cr-jp3').numberRockFloat({
      initNumber: 1155599.0,
      lastNumber: 1155650.61,
      easing: 'linear',
      duration: 17000,
      step: 0.01,
      fixedSize: 2,
    })
  }
  if ($('#fish-jp1').length > 0) {
    $('#fish-jp1').numberRockFloat({
      initNumber: 426629.0,
      lastNumber: 426850.59,
      easing: 'linear',
      duration: 15000,
      step: 0.01,
      fixedSize: 2,
    })
  }

  z.waypointBase()
})

function checkNav(object) {
  if (object.hasClass('not-active')) {
    $('.nav-main').slideDown(300, function () {
      object.removeClass('not-active')
    })
    $('.nav-mask').removeClass('not-active')
    $(document).on('click', function (event) {
      var _con = $('.nav-main')
      if (!_con.is(event.target) && _con.has(event.target).length === 0) {
        $('.nav-main').slideUp(600, function () {
          $('.nav-btn').addClass('not-active')
          $('.nav-mask').addClass('not-active')
        })

        $(document).off()
      }
    })
  } else {
    $('.nav-main').slideUp(300, function () {
      object.addClass('not-active')
    })
    $('.nav-mask').addClass('not-active')
  }
}

var z = {
  waypointBase: function () {
    $('.main-nav ul li').each(function (index, element) {
      var timer = parseInt(50 * index) + 200 + 'ms'
      $(element).css('animation-delay', timer)
    })

    if ($('.quickly-nav .slider-nav').length > 0) {
      $('.quickly-nav .slider-nav').css('animation-delay', '500ms')
    }

    if ($('.game-item').length > 0) {
      $('.game-item').each(function (index, element) {
        $(element).waypoint(
          function () {
            $(element).addClass('animate-scale-in-center')
            var timer = parseInt(100 * index) + 200 + 'ms'
            $(element).css('animation-delay', timer)
          },
          {
            offset: '100%',
          }
        )
      })
    }

    if ($('.post-page').length > 0) {
      $('.post-page').each(function (index, element) {
        $('.post-page').waypoint(
          function () {
            $('.post-page').addClass('animate-fade-in-top')
            var timer = parseInt(100 * index) + 200 + 'ms'
            $(element).css('animation-delay', timer)
          },
          {
            offset: 'bottom-in-view',
          }
        )
      })
    }
  },
}
