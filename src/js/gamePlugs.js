import 'jquery'
import * as Ladda from 'ladda'
import Swal from 'sweetalert2'

// 設定給html可使用
window.$ = window.jQuery = $
window.Swal = Swal
window.Ladda = Ladda

// 頁面載入LOADING;
$(window).on('load', async function () {
  await $('.loading').fadeOut(500)

  await checkSize()
})

// 監聽視窗改變尺寸
window.addEventListener(
  'resize',
  debounce(function (e) {
    checkSize()
  })
)

$(function () {
  if ($('.nav-i-btn').length > 0) {
    $('.nav-i-btn').on('click', function (event) {
      let $this = $(this)
      //取消事件冒泡
      event.stopPropagation()
      checkPersonalNav($this)
    })
  }

  if ($('.nav-btn').length > 0) {
    $('.nav-btn').on('click', function (event) {
      let $this = $(this)
      //取消事件冒泡
      event.stopPropagation()
      checkNav($this)
    })
  }

  /* 跑馬燈*/
  if ($('.game-name').length > 0) {
    $('.game-name').marquee({
      showSpeed: 550,
      pauseSpeed: 3000,
    })
  }
})

function checkPersonalNav(object) {
  if (object.hasClass('not-active')) {
    $('.nav-personal').slideDown(300, function () {
      object.removeClass('not-active')
    })
    $(document).on('click', function (event) {
      var _con = $('.nav-personal')
      if (!_con.is(event.target) && _con.has(event.target).length === 0) {
        $('.nav-personal').slideUp(300, function () {
          $('.nav-i-btn').addClass('not-active')
        })

        $(document).off('click')
      }
    })
  } else {
    $('.nav-personal').slideUp(300, function () {
      object.addClass('not-active')
    })
  }
}

function checkNav(object) {
  if (object.hasClass('not-active')) {
    $('.nav-main').slideDown(300, function () {
      object.removeClass('not-active')
    })

    $(document).on('click', function (event) {
      var _con = $('.nav-main')
      if (!_con.is(event.target) && _con.has(event.target).length === 0) {
        $('.nav-main').slideUp(300, function () {
          $('.nav-btn').addClass('not-active')
        })

        $(document).off('click')
      }
    })
  } else {
    $('.nav-main').slideUp(300, function () {
      object.addClass('not-active')
    })
  }
}

function debounce(func) {
  var timer
  return function (event) {
    if (timer) clearTimeout(timer)
    timer = setTimeout(func, 200, event)
  }
}

function checkSize() {
  var isRotate = $('.game-container').attr('data-rotate')
  if (isRotate) {
    if ($(window).width() <= 768 && $(window).width() < $(window).height()) {
      $('.game-container').addClass('rotate90')
      $('body').addClass('hasRotate90')
      $('.game-container').css({ width: $(window).height(), height: '100vw' })
    } else {
      $('.game-container').removeClass('rotate90')
      $('body').removeClass('hasRotate90')
      $('.game-container').css({ width: '100vw', height: $(window).height() })
    }
  }
}
