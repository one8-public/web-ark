const postcssConfig = {
  plugins: [
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer')({
      overrideBrowserslist: [
        // 指定支援的瀏覽器版本
        'Chrome >= 52',
        'FireFox >= 44',
        'Safari >= 7',
        'Explorer >= 11',
        'last 2 Edge versions',
      ],
    }),
    require('postcss-nested'),
  ],
}

// If we are in production mode, then add cssnano
if (process.env.NODE_ENV === 'production') {
  postcssConfig.plugins.push(require('cssnano'))
}

module.exports = postcssConfig
